const express = require('express');

const router = express.Router();

function multipleOf(val1, val2) {
  let sum = 0;

  sum = parseInt(val1, 10) * parseInt(val2, 10);
  return sum;
}

/* GET home page. */
router.get('/', (req, res, next) => {
  const { val1, val2 } = req.query;

  res.render('newform', { sum: multipleOf(val1, val2) });
});

module.exports = { router, multipleOf };
