const assert = require('assert');
const { sumOf } = require('../routes/calc');
const { multipleOf } = require('../routes/newcalc');

describe('Array', function () {
  describe('#indexOf()', function () {
    it(
      'should return -1 when the value is not present',
      function () {
        assert.equal([1, 2, 3].indexOf(4), -1);
      },
    );
  });
  describe('sumOf()', function () {
    it('should return 2 if val1 and val2 is 1', function () {
      const val1 = '1';
      const val2 = '1';
      const sum = sumOf(val1, val2);
      assert.strictEqual(sum, 2);
    });
  });
  describe('multipleOf()', function () {
    it('should return 16 if val1 and val2 isssdd! 16', function () {
      const val1 = '4';
      const val2 = '4';
      const sum = multipleOf(val1, val2);
      assert.strictEqual(sum, 16);
    });
  });
});
